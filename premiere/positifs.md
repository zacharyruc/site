# Représentation des données : nombres entiers positifs



Les nombres entiers positifs correspondent à l'ensemble des nombres naturels ($`\mathbb N`$). Nous allons voir comment ces nombres sont représentés en machine.



## I. Le binaire

Les tout premiers ordinateurs électroniques, construits dans les années 1940, utilisaient des **tubes à vide** (aussi appelés tubes électroniques ou "lampes") pour représenter les nombres.

![tube à vide](img/tube_a_vide_0.jpg)

*Anecdote : il arrivait régulièrement que des insectes viennent griller sur les tubes à vide, provoquant la rupture de l'ampoule et une panne de la machine. Insecte se dit bug en anglais, d'où l'expression encore utilisée aujourd'hui "avoir un bug".*

L'un des plus célèbres de ces ordinateurs, l'ENIAC, conçu en 1945, utilisait plus de 17000 tubes à vide : il pesait 30 tonnes et occupait une surface de $`167 m^{2}`$ ! Il était ainsi capable de stocker 20 nombres de 10 chiffres.

L'ENIAC utilisait le **système décimal**, aussi appelé **base 10** que vous connaissez bien : c'est celui qui utilise les chiffres de 0 à 9 et que vous utilisez tous les jours pour compter. Pour stocker un chiffre décimal, on avait alors besoin de 10 tubes à vide, numérotés de 0 à 9 :

![décimal - tubes éteints](img/tubes_decimal_eteints.png)

Pour représenter un chiffre, on allumait alors le tube à vide correspondant. Par exemple le chiffre 7 était stocké ainsi :

![décimal - tube 7](img/tubes_decimal_7.png)

> **exercice 1 :**
>
> Combien de tubes étaient nécessaires avec ce système pour représenter un nombre de 10 chiffres ?
>
> ```
> 
> 
> ```

Cette méthode de stockage n'était pas très efficace, puisqu'elle nécessitait énormément de tubes à vide et n'utilisait en réalité qu'une lampe sur 10 pour stocker un chiffre. C'est pourquoi une autre méthode s'est imposée : le **système binaire**, aussi appelé **base 2**. Gottfried Wilhelm Leibniz (XVIIème - XVIIIème siècles) est l'un des précurseurs du binaire tel que nous l'utilisons encore aujourd'hui.

Ce système utilise uniquement les chiffres 0 et 1. Un chiffre binaire est appelé **bit** (*Binary digIT*). Pour stocker un bit, on a besoin d'un seul tube à vide : si le tube est éteint c'est 0, s'il est allumé c'est 1.

Pour stocker un nombre binaire de $`n`$ bits, on avait alors besoin de $`n`$ tubes. Par exemple, le nombre binaire de 10 bits $`1001011101`$ était représenté ainsi :

![binaire - 1001011101](img/tubes_binaire.png)

En utilisant ce système :

* avec $`1`$ bit on peut représenter $`2`$ nombres binaires (0 et 1)
* avec $`2`$ bits on peut représenter $`4`$ nombres binaires (00, 01, 10 et 11)
* avec $`3`$ bits on peut représenter $`8`$ nombres binaires (000, 001, 010, 011, 100, 101, 110 et 111)
* ...
* avec $`n`$ bits on peut représenter **$`2^{n}`$** nombres binaires.

> **exercice 2 :**
>
> Si l'ENIAC avait utilisé le système binaire au lieu du système décimal, combien de nombres aurait-il pu représenter avec 10 tubes à vide ?
>
> ```
> 
> 
> ```

Aujourd'hui, les tubes à vide ont été remplacés par des **transistors**, nous reverrons cela dans le chapitre sur l'architecture des ordinateurs. Cependant, c'est bien le système binaire qui est toujours utilisé : 1 si le transistor laisse passer le courant, 0 sinon.



## II. Conversions d'une base à l'autre

Nous allons maintenant apprendre à convertir un nombre entier positif de la base 2 à la base 10 et inversement.

### 1. Du binaire au décimal

Avant de voir comment faire la conversion de la base 2 à la base 10, étudions un peu la constitution d'un nombre décimal.

Prenons par exemple le nombre 294. Ce nombre est constitué de 3 chiffres :

* 4 est le chiffre des unités : il faut le multiplier par 1
* 9 est le chiffre des dizaines  : il faut le multiplier par 10
* 2 est le chiffre des centaines : il faut le multiplier par 100

Nous pouvons en effet écrire $`294 = 4 \times 1 + 9 \times 10 + 2 \times 100`$.

En remarquant que $`1 = 10^0`$, $`10 = 10^1`$ et $`100 = 10^2`$, on comprend qu'on doit multiplier chaque chiffre par la puissance de 10 correspondant à la position du chiffre. On numérote les positions de chaque chiffre en commençant à 0 et en partant de la droite. Ainsi ici 4 est à la position 0, 9 est à la position 1 et 2 est à la position 2.

![294](img/294.png)

> **exercice 3 :**
>
> Décomposez de la même manière le nombre 1056408.
>
> ```
> 
> 
> 
> 
> ```

Avec le système binaire, c'est le même principe mais avec les puissances de 2.

Reprenons par exemple notre nombre binaire à 10 bits $`1001011101`$. Comme pour le décimal, on numérote les positions de chaque chiffre en commençant à 0 et en partant de la droite. Ainsi :

* le premier 1 de 100101110**1** est à la position 0, on le multiplie par $`2^0`$
* le 0 suivant de 10010111**0**1 est à la position 1, on le multiplie par $`2^1`$
* le 1 suivant de 1001011**1**01 est à la position 2, on le multiplie par $`2^2`$
* ...
* le 1 final  de **1**001011101 est à la position 9, on le multiplie par $`2^9`$

Puis comme avec le décimal, on additionne le tout pour retrouver notre nombre : $`1001011101 = 1 \times 2^0 + 0 \times 2^1 + 1 \times 2^2 + 1 \times 2^3 + 1 \times 2^4 + 0 \times 2^5 + 1 \times 2^6 + 0 \times 2^7 + 0 \times 2^8 + 1 \times 2^9 = 1 \times 1 + 0 \times 2 + 1 \times 4 + 1 \times 8 + 1 \times 16 + 0 \times 32 + 1 \times 64 + 0 \times 128 + 0 \times 256 + 1 \times 512 = 1 + 0 + 4 + 8 + 16 + 0 + 64 + 0 + 0 + 512 = 605`$.

![1001011101](img/1001011101.png)

Nous avons finalement trouvé que le nombre binaire $`1001011101`$ correspondait au nombre décimal 605.

Il est en réalité faux d'écrire $`1001011101 = 605`$.  Il est impératif de préciser la base utilisée en indice après le nombre. On doit donc écrire $`1001011101_2 = 605`$.

*Remarque : par défaut, si on ne met pas l'indice cela signifie que c'est la base 10 qui est utilisée.*

> **exercice 4 :**
>
> Convertissez en base 10 les nombres binaires suivants :
>
> * $`100_2`$
> * $`10101_2`$
> * $`11111111_2`$
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

En binaire, on ne dit pas chiffre des unités, chiffre des dizaines, chiffres des centaines... Par contre, un nombre binaire possède 2 bits qui ont un nom particulier : le bit à la plus grande position (tout à gauche) est dit **bit de poids fort** et celui à la plus petite position (tout à droite) est dit **bit de poids faible**.

> **exercice 5 :**
>
> Quels sont les bits de poids fort et de poids faible des nombres binaires suivants ?
>
> * $`100_2`$
> * $`1001011101_2`$
>
> ```
> 
> 
> ```

### 2. Du décimal au binaire

Pour compter avec la base 10, nous utilisons d'abord une seule position (le chiffre des unités) : cela nous permet de compter de 0 à 9. Puis pour écrire le nombre suivant, nous avons besoin d'ajouter une seconde position (le chiffre des dizaines) : on remet le chiffre des unités à 0, et on ajoute le 1 à gauche, cela donne 10. On recommence ensuite à augmenter le chiffre des unités, pour avoir 11, puis 12, ...

| chiffre des dizaines | chiffre des unités |                         explications                         |
| :------------------: | :----------------: | :----------------------------------------------------------: |
|                      |         0          |             le premier chiffre disponible est 0              |
|                      |         1          |              le second chiffre disponible est 1              |
|                      |         2          |            le troisième chiffre disponible est 2             |
|                      |         3          |                             ...                              |
|                      |         4          |                             ...                              |
|                      |         5          |                             ...                              |
|                      |         6          |                             ...                              |
|                      |         7          |                             ...                              |
|                      |         8          |                             ...                              |
|                      |         9          |             le dernier chiffre disponible est 9              |
|          1           |         0          | nous n'avons plus de chiffre disponible, on passe à la position suivante : 1 pour le chiffre des dizaines, et le premier chiffre disponible pour les unités redevient 0 |
|          1           |         1          |      le second chiffre disponible pour les unités est 1      |
|          1           |         2          |    le troisième chiffre disponible pour les unités est 2     |
|         ...          |        ...         |                             ...                              |
|          1           |         9          |     le dernier chiffre disponible pour les unités est 9      |
|          2           |         0          | nous n'avons plus de chiffre disponible pour les unités, on regarde la position suivante : le prochain chiffre disponible pour les dizaines est le 2, et le premier chiffre disponible pour les unités redevient 0 |
|         ...          |        ...         |                      et ainsi de suite                       |

Pour le système binaire, cela fonctionne de la même manière : nous disposons de deux chiffres (0 et 1), et lorsqu'on a plus de chiffre disponible, on passe à la position suivante.

| base 10 | base 2 |                         explications                         |
| :-----: | :----: | :----------------------------------------------------------: |
|    0    |   0    |             le premier chiffre disponible est 0              |
|    1    |   1    |              le second chiffre disponible est 1              |
|    2    |   10   | nous n'avons plus de chiffre disponible, on passe à la position suivante : 1 pour la position 1, et la position 0 redevient 0 |
|    3    |   11   |    le second chiffre disponible pour la position 0 est 1     |
|    4    |  100   | nous n'avons plus de chiffre disponible pour la position 0, on regarde la position 1 : plus de chiffre disponible non plus donc on passe à la position suivante : 1 pour la position 2, et le premier chiffre disponible pour les position 1 et position 0 redevient 0 |
|    5    |  101   |    le second chiffre disponible pour la position 0 est 1     |
|    6    |  110   | nous n'avons plus de chiffre disponible pour la position 0, on regarde la position 1 : le second chiffre disponible pour la position 1 est 1 et le premier chiffre disponible pour la position 0 redevient 0 |
|   ...   |  ...   |                             ...                              |

> **exercice 6 :**
>
> Continuez de la même manière pour trouver l'écriture binaire des nombres décimaux 7, 8, 9, 10, 11, 12, 13, 14 et 15.
>
> ```
> 
> 
> 
> 
> 
> ```

On se rend vite compte que cette méthode ne va pas être très pratique pour trouver l'écriture binaire de grands nombres. Il existe plusieurs manières de convertir un nombre de la base 10 à la base 2, nous allons voir la plus simple : la **méthode par divisions euclidiennes successives**.

Pour cela, on prend notre nombre décimal de départ et on effectue une division euclidienne par 2. Comme vous l'avez appris en primaire, on obtient un quotient et un reste. Par exemple avec le nombre 605 :

![605 division](img/605.png)

On garde de côté le reste, et on recommence la division euclidienne par 2 avec le quotient. Dans notre exemple, on met de côté le reste $`1`$ et on divise $`302`$ par 2. On continue ensuite de la même manière jusqu'à ce que le quotient soit 0. Pour retrouver l'écriture binaire de notre nombre, il suffit ensuite de prendre tous les restes obtenus **de bas en haut**.

![605 divisions](img/605_complet.png)

En remontant les restes obtenus, on retrouve bien $`605 = 1001011101_2`$.

> **exercice 7 :**
>
> Justifiez que le reste obtenu en divisant n'importe quel nombre par 2 est toujours 0 ou 1.
>
> ```
> 
> 
> 
> ```

> **exercice 8 :**
>
> Trouvez l'écriture binaire des nombres décimaux suivants :
>
> * 61
>
> * 184
>
> * 255
> * 256
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

Cette méthode de conversion du décimal en binaire est très utilisée en informatique car elle s'écrit facilement sous forme d'un algorithme.

> **exercice 9 :**
>
> Complétez l'algorithme suivant, qui permet de trouver l'écriture binaire d'un nombre décimal :
>
> ```
> Entrée :
> 	un nombre entier positif n
> Algorithme :
> 	n_binaire <- une chaîne de caractères vide
> 	tant que ............ :
> 		n_binaire <- ............
> 		n <- ............
> 	fin tant que
> Sortie :
> 	la chaîne de caractère n_binaire
> ```

> **Pour aller plus loin :**
>
> S'il vous reste du temps, vous pouvez aller voir une autre méthode de conversion sur ce lien : [https://lehollandaisvolant.net/tuto/bin/#db1](https://lehollandaisvolant.net/tuto/bin/#db1).
>
> Cette méthode peut dans certains cas être plus rapide pour les conversions à la main, mais elle est moins efficace en informatique car les calculs de puissances sont longs.
>
> Vous pouvez par exemple tester cette méthode sur les nombres de l'exercice 8.
>
> ```
> 
> 
> 
> 
> 
> ```



## III. Les données en machine

### 1. Taille des données

Vous savez désormais que les données sont stockées dans la mémoire de votre machine sous forme de bits. Les données de base sont représentées d'une manière dépendante de leur nature, que l'on appelle **codage**. Nous venons donc de voir le codage des nombres entiers positifs en machine. Nous aurons l'occasion d'étudier le codage des nombres entiers relatifs, celui des nombres réels ainsi que le codage des caractères lors de prochaines séances. Le codage conditionne la **taille** des différentes valeurs en mémoire.

Pour mesurer la place qu'occupe les données, on ne compte pas le nombre de bits, on compte en réalité le nombre d'**octets**. Un octet est un groupe de 8 bits.

> **exercice 10 :**
>
> Combien de nombres entiers positifs peut-on coder avec un octet ?
>
> ```
> 
> 
> ```
>
> Quel est donc le plus grand entier positif représentable avec un octet ?
>
> ```
> 
> 
> ```

Voici les principales unités de mesure de taille des données :

* un octet = 8 bits
* un kilo-octet (ko) = 1000 octets = $`10^3`$ octets
* un méga-octet (Mo) = 1000 kilo-octets = $`10^6`$ octets
* un giga-octet (Go) = 1000 méga-octets = $`10^9`$ octets
* un téra-octet (To) = 1000 giga-octets = $`10^{12}`$ octets

Pour vous donner une idée, un fichier texte se mesure généralement en ko, un fichier image en Mo et un fichier vidéo en Go.

> **exercice 11 :**
>
> Téléchargez le fichier de ce cours sur votre machine :
>
> ![télécharger cours](img/download.png)
>
> Quelle est l'ordre de grandeur de la taille du fichier (ko, Mo, Go, To) ? Quelle est la taille en octets ? Quelle est donc la taille en nombre de bits ?
>
> ```
> 
> 
> 
> 
> ```

Il peut arriver que le nombre de bits d'un fichier ne soit pas un multiple de 8. Dans ce cas, on ajoute des 0 à gauche jusqu'à arriver à un multiple de 8. Par exemple, $`101`$ devient $`00000101`$, et $`110011001010`$ devient  $`00001100 \; 11001010`$.

> **exercice 12 :**
>
> Donnez les octets représentant les nombres suivants :
>
> * 61
> * 605
>
> ```
> 
> 
> ```

> **exercice 13 :**
>
> Combien d'octets sont nécessaires pour représenter les nombres suivants ?
>
> * 180
> * 974
> * 512
>
> ```
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> En informatique, on a l'habitude de compter en puissances de 2 et non en puissances de 10. On peut donc quelque fois trouver d'autres mesures pour la taille des données :
>
> * un octet = 8 bits
> * un kibi-octet (kio) =  $`2^{10}`$ octets
> * un mébi-octet (Mio) = $`2^{10}`$ kibi-octets = $`2^{20}`$ octets
> * un gibi-octet (Gio) = $`2^{10}`$ mébi-octets = $`2^{30}`$ octets
> * un tébi-octet (Tio) = $`2^{10}`$ gibi-octets = $`2^{40}`$ octets
>
> Certains logiciels font la confusion entre ces 2 systèmes de mesure : par exemple, ils indiquent qu'un fichier possède 15 ko alors qu'il possède en réalité 15 kio. En quoi cela peut-il poser problème ?
>
> ```
> 
> 
> 
> ```

### 2. L'hexadécimal

En machine, toutes les données sont en binaire : parfait pour les signaux électriques, mais pas très pratique pour nous lorsqu'on veut lire ces données. On utilise la base 10 au quotidien, mais vous venez de voir que la conversion n'est pas immédiate. Il est en réalité beaucoup plus facile d'utiliser un codage avec une base multiple de 2. On utilise donc souvent la **base 16**, appelé **système hexadécimal** (hexa = 6, déci = 10, 6 + 10 = 16) car 16 est un multiple de 2, et qu'on peut alors représenter 8 bits avec seulement 2 chiffres.

En base 10, on utilise 10 chiffres : 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9. En base 2, on utilise 2 chiffres : 0 et 1. En base 16, il faut donc 16 chiffres : on prend les chiffres de 0 à 9 comme pour le décimal, puis on continue avec les lettres de l'alphabet : 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E, et F. 

Voici donc la table de correspondances entre les bases 2, 10 et 16 :

| Binaire (base 2) | Décimal (base 10) | Hexadécimal (base 16) |
| ---------------- | ----------------- | --------------------- |
| 0                | 0                 | 0                     |
| 1                | 1                 | 1                     |
| 10               | 2                 | 2                     |
| 11               | 3                 | 3                     |
| 100              | 4                 | 4                     |
| 101              | 5                 | 5                     |
| 110              | 6                 | 6                     |
| 111              | 7                 | 7                     |
| 1000             | 8                 | 8                     |
| 1001             | 9                 | 9                     |
| 1010             | 10                | A                     |
| 1011             | 11                | B                     |
| 1100             | 12                | C                     |
| 1101             | 13                | D                     |
| 1110             | 14                | E                     |
| 1111             | 15                | F                     |

Tous les chiffres hexadécimaux correspondent donc à un nombre binaire de 4 bits (en ajoutant éventuellement des 0 devant).

Pour convertir un nombre binaire en hexadécimal, on regroupe donc les bits en groupes de 4, et chaque groupe donne un chiffre hexadécimal. Par exemple, $`11001000_2`$ découpé en groupes de 4 donne $`1100 \; 1000`$. $`1100_2`$ vaut 12 en décimal, ce qui correspond à la lettre C en hexadécimal. $`1000_2`$  correspond au 8. On peut donc écrire $`11001000_2 = C8_{16}`$.

*Remarque : comme pour le binaire, il ne faut pas oublier de préciser la base utilisée en notant 16 en indice après le nombre.*

Si jamais le nombre binaire n'est pas un multiple de 4, on ajoute à nouveau des 0 devant.

> **exercice 14 :**
>
> Convertissez les nombres binaires suivants en base 16 :
>
> * $`10010011_2`$
> * $`1110001_2`$
> * $`10111010110010_2`$
>
> ```
> 
> 
> 
> 
> 
> ```

Pour convertir un nombre hexadécimal, c'est la même chose mais dans l'autre sens : on remplace chaque lettre par les 4 bits qui correspondent. Par exemple pour $`A7_{16}`$ : $`A_{16} = 1010_2`$ et $`7_{16} = 0111_2`$ donc $`A7_{16} = 10100111_2`$.

*Attention : on remplace bien 7 par 0111 sur **4 bits** et non pas par 111.*

> **exercice 15 :**
>
> Convertissez les nombres hexadécimaux suivants en base 2 :
>
> * $`2B_{16}`$
> * $`D6_{16}`$
> * $`55_{16}`$
> * $`7F159A_{16}`$
>
> ```
> 
> 
> 
> 
> 
> 
> ```

Quand on lit les données d'un fichier, on va donc préférer utiliser l'hexadécimal plutôt que le binaire (moins long, et la conversion est rapide). Il reste cependant quelque fois utile de reconvertir l'hexadécimal en décimal.

Pour convertir de la base 16 à la base 10, c'est le même principe que la conversion de la base 2 à la base 10 mais avec les puissances de 16. Par exemple avec $`42BF_{16}`$ :

![42BF](img/42BF.png)

Il faut juste retenir qu'il faut remplacer les lettres par le nombre décimal correspondant dans le calcul (A par 10, B par 11, C par 12, D par 13, E par 14 et F par 15).

> **exercice 16 :**
>
> Convertissez les nombres hexadécimaux suivants en base 10 :
>
> * $`B2_{16}`$
> * $`6D_{16}`$
> * $`55_{16}`$
> * $`17FA_{16}`$
>
> ```
> 
> 
> 
> 
> 
> 
> 
> ```

Pour finir, pour convertir un nombre décimal en base 16, c'est aussi le même principe que pour les conversions du décimal au binaire : les divisions euclidiennes successives, mais on divise par 16 et non plus par 2. Par exemple avec $`17087`$ :

![17087](img/17087.png)

À nouveau, on remplace les nombres par la lettre correspondante, et en remontant les restes on trouve bien $`17087 = 42BF_{16}`$

> **exercice 17 :**
>
> Convertissez les nombres décimaux suivants en base 16 :
>
> * 281
> * 1025
> * 694
>
> ```
> 
> 
> 
> 
> 
> 
> 
> ```

### 3. Autres bases

D'autres bases que le binaire, le décimal et l'hexadécimal peuvent parfois être utilisées.

La méthode de conversion d'une base B ($`B \geq 2`$)  à la base 10 est toujours la même : on multiplie chaque chiffre par les puissances de B : puissances de 2 pour le binaire, puissances de 16 pour l'hexadécimal...

La méthode de conversion de la base 10 à une base B ($`B \geq 2`$) est toujours la même aussi : les divisions euclidiennes successives : divisions par 2 pour le binaire, divisions par 16 pour l'hexadécimal...

> **Pour aller plus loin :**
>
> Vous pouvez regarder cette vidéo qui reprend ces méthodes de conversion pour le système octal (la base 8) : [https://www.youtube.com/watch?v=8SS0BlIvdmU](https://www.youtube.com/watch?v=8SS0BlIvdmU)
>
> Convertissez les nombres décimaux suivants en base 8 :
>
> * 157
> * 862
>
> ```
> 
> 
> 
> 
> 
> 
> ```
>
> Convertissez les nombres octaux suivants en base 10 :
>
> * $`147_8`$
> * $`613425_8`$
>
> ```
> 
> 
> 
> 
> ```



---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*, [Wikipedia](https://en.wikipedia.org/wiki/Vacuum_tube)

