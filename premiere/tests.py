def somme(entier1, entier2):
    """
    Calcule la somme de deux entiers.
    :param entier1: (int) le premier entier
    :param entier1: (int) le premier entier
    :return: (int) la somme

    >>> somme(2, 5)
    7
    >>> somme(2, -5)
    -3
    >>> somme(-2, 5)
    3
    >>> somme(-2, -5)
    -7
    >>> somme(0, 0)
    0
    """
    resultat = entier1 + entier2
    return resultat

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
