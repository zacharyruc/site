import turtle
import random
#permets de créé un carré pour faire les faces
def carre():
    turtle.hideturtle()
    turtle.down()
    turtle.pencolor("black")
    for loop in range(4):
        turtle.forward(60)
        turtle.right(90)
#créé le f
def de5 ():
    turtle.up()
    turtle.goto(0, 0)
    turtle.down()
    carre()
#cercle1
    turtle.up()
    turtle.goto(30, -35)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    y = turtle.heading
#cercle2
    turtle.up()
    turtle.goto(15, -17.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    x = turtle.heading()
#cercle3
    turtle.up()
    turtle.goto(45, -52.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    turtle.up()
#cercle4
    turtle.up()
    turtle.goto(45, -17.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    x = turtle.heading()
    turtle.end_fill()
    x = turtle.heading()
#cercle5
    turtle.up()
    turtle.goto(15, -52.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    x = turtle.heading()
    turtle.end_fill()
    x = turtle.heading()
def de4 ():
    turtle.up()
    turtle.goto(0, -100)
    turtle.down()
    carre()
#cercle2
    turtle.up()
    turtle.goto(15, -117.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    x = turtle.heading()
#cercle3
    turtle.up()
    turtle.goto(45, -152.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    turtle.up()
#cercle4
    turtle.up()
    turtle.goto(45, -117.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    x = turtle.heading()
    turtle.end_fill()
    x = turtle.heading()
#cercle5
    turtle.up()
    turtle.goto(15, -152.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    x = turtle.heading()
    turtle.end_fill()
    x = turtle.heading()
def de6 ():
    turtle.up()
    turtle.goto(0, -200)
    turtle.down()
    carre()
#cercle2
    turtle.up()
    turtle.goto(15, -217.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    x = turtle.heading()
#cercle3
    turtle.up()
    turtle.goto(45, -252.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    turtle.up()
#cercle4
    turtle.up()
    turtle.goto(45, -217.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    x = turtle.heading()
    turtle.end_fill()
    x = turtle.heading()
#cercle5
    turtle.up()
    turtle.goto(15, -252.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    x = turtle.heading()
    turtle.end_fill()
    x = turtle.heading()
#cercle6
    turtle.up()
    turtle.goto(15, -235)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    x = turtle.heading()
    turtle.end_fill()
    x = turtle.heading()
#cercle7
    turtle.up()
    turtle.goto(45, -235)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    x = turtle.heading()
    turtle.end_fill()
    x = turtle.heading()
def de3 ():
    turtle.up()
    turtle.goto(0, -300)
    turtle.down()
    carre()
#cercle1
    turtle.up()
    turtle.goto(30, -335)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    y = turtle.heading
#cercle2
    turtle.up()
    turtle.goto(15, -317.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    x = turtle.heading()
#cercle3
    turtle.up()
    turtle.goto(45, -352.5)
    turtle.fillcolor("black")
    turtle.down()
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    turtle.up()
    

turtle.down()
turtle.pencolor("black")
def de1():
    turtle.up()
    turtle.goto(0, -400)
    turtle.down()
    carre()    
    turtle.up()
    turtle.forward(25)
    turtle.right(90)
    turtle.forward(30)
    turtle.down()
    turtle.fillcolor("black")
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    turtle.up()
    turtle.forward(100)
    turtle.down()
def de2():
    turtle.up()
    turtle.goto(0, -500)
    turtle.down()
    carre ()
    turtle.up()
    turtle.fillcolor("black")
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    turtle.up()
    turtle.forward(45)
    turtle.left(90)
    turtle.forward(45)
    turtle.down()
    turtle.fillcolor("black")
    turtle.begin_fill()
    turtle.circle(5)
    turtle.end_fill()
    turtle.up()

def tirage_j1():
    resultat = 0
    resultat = random.randint(1, 6)
    if resultat == 1:
        de1()
    elif resultat == 2:
        de2()
    elif resultat == 3:
        de3()
    elif resultat == 4:
        de4()
    elif resultat == 5:
        de5()
    elif resultat == 6:
        de6()
    return resultat
        
def tirage_j2():
    resultat2 = 0
    resultat2 = random.randint(1, 6)
    if resultat2 == 1:
        de1()
    elif resultat2 == 2:
        de2()
    elif resultat2 == 3:
        de3()
    elif resultat2 == 4:
        de4()
    elif resultat2 == 5:
        de5()
    elif resultat2 == 6:
        de6()
    return resultat2
    
def jouer_manche1():
    resultat = tirage_j1()
    resultat2 = tirage_j2()

#résultat des églitée
    if resultat == 1 and resultat2 == 1:
        print ("égalité")
        return ("égalité")
    elif resultat == 2 and resultat2 == 2:
        print ("égalité")
        return ("égalité")
    elif resultat == 3 and resultat2 == 3:
        print ("égalité")
        return ("égalité")
    elif resultat == 4 and resultat2 == 4:
        print ("égalité")
        return ("égalité")
    elif resultat == 5 and resultat2 == 5:
        print ("égalité")
        return ("égalité")
    elif resultat == 6 and resultat2 == 6:
        print ("égalité")
        return ("égalité")
#résultat avec face 1 du dé
    elif resultat == 1 and resultat2 == 2:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 1 and resultat2 == 3:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 1 and resultat2 == 4:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 1 and resultat2 == 5:
        print ("joueur 2 gagnant")
    elif resultat == 1 and resultat2 == 6:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
#resultat avec face 2 du dé
    elif resultat == 2 and resultat2 == 1:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 2 and resultat2 == 3:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 2 and resultat2 == 4:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 2 and resultat2 == 5:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 2 and resultat2 == 6:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
#resultat avec face 3 du dé
    elif resultat == 3 and resultat2 == 1:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 3 and resultat2 == 2:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 3 and resultat2 == 4:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 3 and resultat2 == 5:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 3 and resultat2 == 6:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
#resultat avec face 4 du dé
    elif resultat == 4 and resultat2 == 1:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 4 and resultat2 == 2:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 4 and resultat2 == 3:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 4 and resultat2 == 5:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 4 and resultat2 == 6:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
#resultat avec face 5 du dé
    elif resultat == 5 and resultat2 == 1:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 5 and resultat2 == 2:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 5 and resultat2 == 3:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 3 and resultat2 == 4:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 3 and resultat2 == 6:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
#resultat avec face 6 du dé
    elif resultat == 6 and resultat2 == 1:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 6 and resultat2 == 2:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 6 and resultat2 == 3:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 6 and resultat2 == 4:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 6 and resultat2 == 5:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
        
#définis le résultat d'une manche
def résultat_manche1():
    resultat_manche = jouer_manche1

def jouer_manche2():
    turtle.clear()
    resultat = tirage_j1()
    resultat2 = tirage_j2()
    
#résultat des églitée
    if resultat == 1 and resultat2 == 1:
        print ("égalité")
        return ("égalité")
    elif resultat == 2 and resultat2 == 2:
        print ("égalité")
        return ("égalité")
    elif resultat == 3 and resultat2 == 3:
        print ("égalité")
        return ("égalité")
    elif resultat == 4 and resultat2 == 4:
        print ("égalité")
        return ("égalité")
    elif resultat == 5 and resultat2 == 5:
        print ("égalité")
        return ("égalité")
    elif resultat == 6 and resultat2 == 6:
        print ("égalité")
        return ("égalité")
#résultat avec face 1 du dé
    elif resultat == 1 and resultat2 == 2:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 1 and resultat2 == 3:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 1 and resultat2 == 4:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 1 and resultat2 == 5:
        print ("joueur 2 gagnant")
    elif resultat == 1 and resultat2 == 6:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
#resultat avec face 2 du dé
    elif resultat == 2 and resultat2 == 1:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 2 and resultat2 == 3:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 2 and resultat2 == 4:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 2 and resultat2 == 5:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 2 and resultat2 == 6:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
#resultat avec face 3 du dé
    elif resultat == 3 and resultat2 == 1:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 3 and resultat2 == 2:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 3 and resultat2 == 4:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 3 and resultat2 == 5:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 3 and resultat2 == 6:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
#resultat avec face 4 du dé
    elif resultat == 4 and resultat2 == 1:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 4 and resultat2 == 2:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 4 and resultat2 == 3:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 4 and resultat2 == 5:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 4 and resultat2 == 6:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
#resultat avec face 5 du dé
    elif resultat == 5 and resultat2 == 1:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 5 and resultat2 == 2:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 5 and resultat2 == 3:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 3 and resultat2 == 4:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 3 and resultat2 == 6:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
#resultat avec face 6 du dé
    elif resultat == 6 and resultat2 == 1:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 6 and resultat2 == 2:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 6 and resultat2 == 3:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 6 and resultat2 == 4:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
    elif resultat == 6 and resultat2 == 5:
        print ("joueur 2 gagnant")
        return ("joueur 2 gagnant")
#défini le résultat de la manche 2 s'aditionnant au réssultat de la manche 1
def résultat_total():
    resultat_total = 0
def résultat_manche2():
    résultat_manche1 = jouer_manche1() + jouer_manche2()
    
    if résultat_manche1 == "joueur 2 gagnant" + "joueur 2 gagnant":
        print ("joueur 1: 0pts ; joueur2: 2pts")
        return ("joueur 1: 0pts ; joueur2: 2pts")
    elif résultat_manche1 == "joueur 1 gagnant" + "joueur 1 gagnant":
        print ("joueur 1: 2pts ; joueur2: 0pts")
        return ("joueur 1: 2pts ; joueur2: 0pts")
    elif résultat_manche1 == "joueur 2 gagnant" + "joueur 1 gagnant":
        print ("joueur 1: 1pts ; joueur2: 1pts")
        return ("joueur 1: 1pts ; joueur2: 1pts")
    elif résultat_manche1 == "égalité" + "égalité":
        print ("joueur 1: 1pts ; joueur2: 1pts")
        return ("joueur 1: 1pts ; joueur2: 1pts")
    
résultat_total = résultat_manche2()



