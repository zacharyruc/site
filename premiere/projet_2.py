# PROJET FILTRES
# Binôme :
# - zachary ruckebusch
# - leo assoumani

from PIL import Image #importe la bibliothèque Python PIL (Python Image Library)

def augmente_luminosite(nom_image): 
    """défini la fonction qui va permettre d'augmenter la luminosité avec un paramètre str"""
    original = Image.open(nom_image) #Ouvre le fichier "nom_image" et le stocke dans la variable image.
    largeur = original.width #Retourne la largeur de l’image en pixels.
    hauteur = original.height #Retourne la hauteur de l’image en pixels.
    image = Image.new("RGB", (largeur, hauteur)) #Crée une nouvelle image de largeur larg et de hauteur haut et la stocke dans la variable image.
    
    for x in range(largeur): #défini la largeur
        for y in range(hauteur): #défini la hauteur
            (r, g, b) = original.getpixel((x, y)) #Retourne la couleur du pixel (x, y) de l'image original.
            image.putpixel((x, y), (r+100, g+100, b+100)) #Modifie la couleur du pixel (x, y) de image en (r, g, b).
    
    return image #renvoi image

def filtre_rouge(nom_image): 
    """défini la fonction qui va permettre de généré un filtre rouge avec un paramètre str"""
    original = Image.open(nom_image) #Ouvre le fichier "nom_image" et le stocke dans la variable image.
    largeur = original.width #Retourne la largeur de l’image en pixels.
    hauteur = original.height #Retourne la hauteur de l’image en pixels.
    image = Image.new("RGB", (largeur, hauteur)) #Crée une nouvelle image de largeur larg et de hauteur haut et la stocke dans la variable image.
    
    for x in range(largeur): #défini la largeur
        for y in range(hauteur): #défini la hauteur
            (r, g, b) = original.getpixel((x, y)) #Retourne la couleur du pixel (x, y) de l'image original.
            image.putpixel((x, y), (r+100, g+0, b+0)) #Modifie la couleur rouge du pixel (x, y) de image en (r, g, b).
    
    return image #renvoi image

def filtre_vert(nom_image): 
    """défini la fonction qui va permettre de généré un filtre vert avec un paramètre str"""
    original = Image.open(nom_image) #Ouvre le fichier "nom_image" et le stocke dans la variable image.
    largeur = original.width #Retourne la largeur de l’image en pixels.
    hauteur = original.height #Retourne la hauteur de l’image en pixels.
    image = Image.new("RGB", (largeur, hauteur)) #Crée une nouvelle image de largeur larg et de hauteur haut et la stocke dans la variable image.
    
    for x in range(largeur): #défini la largeur
        for y in range(hauteur): #défini la hauteur
            (r, g, b) = original.getpixel((x, y)) #Retourne la couleur du pixel (x, y) de l'image original.
            image.putpixel((x, y), (r+0, g+100, b+0)) #Modifie la couleur verte du pixel (x, y) de image en (r, g, b).
    
    return image #renvoi image

def filtre_bleu(nom_image): 
    """défini la fonction qui va permettre de généré un filtre bleu avec un paramètre str"""
    original = Image.open(nom_image) #Ouvre le fichier "nom_image" et le stocke dans la variable image.
    largeur = original.width #Retourne la largeur de l’image en pixels.
    hauteur = original.height #Retourne la hauteur de l’image en pixels.
    image = Image.new("RGB", (largeur, hauteur)) #Crée une nouvelle image de largeur larg et de hauteur haut et la stocke dans la variable image.
    
    for x in range(largeur): #défini la largeur
        for y in range(hauteur): #défini la hauteur
            (r, g, b) = original.getpixel((x, y)) #Retourne la couleur du pixel (x, y) de l'image original.
            image.putpixel((x, y), (r+0, g+0, b+100)) #Modifie la couleur bleu du pixel (x, y) de image en (r, g, b).
    
    return image #renvoi image

def filtre_gris(nom_image): 
    """défini la fonction qui va permettre de généré un filtre gris avec un paramètre str"""
    original = Image.open(nom_image) #Ouvre le fichier "nom_image" et le stocke dans la variable image.
    largeur = original.width #Retourne la largeur de l’image en pixels.
    hauteur = original.height #Retourne la hauteur de l’image en pixels.
    image = Image.new("RGB", (largeur, hauteur)) #Crée une nouvelle image de largeur larg et de hauteur haut et la stocke dans la variable image.

    for x in range(largeur): #défini la largeur
        for y in range(hauteur): #défini la hauteur
            (r, g, b) = original.getpixel((x, y)) #Retourne la couleur du pixel (x, y) de l'image original.
            moyenne = (r + b + g) //3
            image.putpixel((x, y), (moyenne, moyenne, moyenne)) #Modifie la couleur au gris du pixel (x, y) de image en (r, g, b).
    
    return image #renvoi image

def filtre_noir_blanc(nom_image): 
    """défini la fonction qui va permettre de généré un filtre noir et blanc avec un paramètre str"""
    original = Image.open(nom_image) #Ouvre le fichier "nom_image" et le stocke dans la variable image.
    largeur = original.width #Retourne la largeur de l’image en pixels.
    hauteur = original.height #Retourne la hauteur de l’image en pixels.
    image = Image.new("RGB", (largeur, hauteur)) #Crée une nouvelle image de largeur larg et de hauteur haut et la stocke dans la variable image.

    for x in range(largeur): #défini la largeur
        for y in range(hauteur): #défini la hauteur
            (r, g, b) = original.getpixel((x, y)) #Retourne la couleur du pixel (x, y) de l'image original.
            moyenne = (r + g + b) // 3 #calcule la moyenne
            if moyenne > 128: #si moyenne supérieur à 128
                A = 255 #mets la valeur A à 255
            else : #sinon
                A = 0
            image.putpixel((x, y), (A, A, A)) #Modifie la couleur au noir et blanc du pixel (x, y) de image en (r, g, b).
    
    return image #renvoi image


def interface():
    """défini la fonction interface, sans paramètre"""
    nom_image = input("Nom de l'image à utiliser ?") #demande à l'utilisateur l'image à utiliser
    
    print("Voici les filtres disponibles :") #écrit: Voici les filtres disponibles
    print("1 - rouge") #écrit: 1 - rouge
    print("2 - vert") #écrit: 2 - vert
    print("3 - bleu") #écrit: 3 - bleu
    print("4 - augmenter luminosité") #écrit: 4 - augmenter luminosité
    print("5 - gris") #écrit: 5 - gris
    print("6 - noir et blanc") #écrit: 6 - noir et blanc
    filtre = input("Quel filtre voulez-vous appliquer ? ") #demande à l'utilisateur quel est le filtre à appliquer
    
    if filtre == "1":
        image = filtre_rouge(nom_image) #affecte le filtre rouge à la réponse 1
    if filtre == "2":
        image = filtre_vert(nom_image) #affecte le filtre vert à la réponse 2
    if filtre == "3":
        image = filtre_bleu(nom_image) #affecte le filtre bleu à la réponse 3
    if filtre == "4":
        image = augmente_luminosite(nom_image) #affecte le filtre augment luminausité à la réponse 4
    if filtre == "5":
        image = filtre_gris(nom_image) #affecte le filtre gris à la réponse 5
    if filtre == "6":
        image = filtre_noir_blanc(nom_image) #affecte lefiltre noir et blanc à la réponse 6
        
    
    print("Le filtre est appliqué.") #écrit: Le filtre est appliqué.
    print("Vous pouvez :") #écrit: Vous pouvez :
    print("1 - sauvegarder l'image") #écrit: 1 - sauvegarder l'image
    print("2 - ouvrir l'image") #écrit: 2 - ouvrir l'image
    sauvegarde = input("Que souhaitez-vous faire ? ") #demande à l'utilisateur ce qu'il veut faire
    
    if sauvegarde == "1": # si la réponse est 1
        sauvegarde = input("Quelle est le nom du fichier ? ") #demande à l'utilisateur Quelle est le nom du fichier
        image.save("photo.jpg") #enregistre l'image
    elif sauvegarde == "2": #sinon si la réponse est 2
        image.show() #montre l'image
    else: #sinon
       return image #retourne image