life_actions = [];

switch (playerSide) do {
    case civilian: {
        life_actions pushBack (player addAction[localize "STR_pAct_DropFishingNet",life_fnc_dropFishingNet,"",0,false,false,"",'(surfaceisWater (getPos vehicle player)) && (vehicle player isKindOf "Ship") && life_carryWeight < life_maxWeight && speed (vehicle player) < 2 && speed (vehicle player) > -1 && !life_net_dropped ']);
        life_actions pushBack (player addAction[localize "STR_pAct_RobPerson",life_fnc_robAction,"",0,false,false,"",'!isNull cursorObject && player distance cursorObject < 3.5 && isPlayer cursorObject && animationState cursorObject == "Incapacitated" && !(cursorObject getVariable ["robbed",false]) ']);
        life_actions pushBack (player addAction["<t color='#0099FF'>S'asseoir</t>",{[cursorObject,player] execVM "script\Sitting\sitdown.sqf"},true,1,true,true,"",' player distance cursorObject < 3 && {(("bench" in (str cursorObject)) || ("chair" in (str cursorObject)))} ']);


		life_actions pushBack (player addAction ["<t color='#14F51C'>Prendre les fonds</t>",{ life_inv_monnaietraiter = life_inv_monnaietraiter + 1; hint "Vous avez reçu 1 sac de monnaie"; },"",0,false,false,"",'((typeOf cursorObject) == "Kitten_Coffre_Fort") && license_civ_convoyeur && life_inv_cle > 0']);

        
	};

    case west: {
        life_actions pushBack (player addAction["<t color='#FF0000'>Saisir l'objet</t>",{deleteVehicle cursorObject},"",0,false,false,"","cursorObject getVariable ['poserbyplayer',false]"]);
        life_actions pushBack (player addAction["<t color='#266ff7'>Déverouiller le véhicule</t>",{cursorObject lock false;},"",0,false,false,"",'((cursorObject isKindOf "Car") && ((locked cursorObject) isEqualTo 2))']);
		life_actions pushBack (player addAction["<t color='#266ff7'>Vérouiller le véhicule</t>",{cursorObject lock true;},"",0,false,false,"",'((cursorObject isKindOf "Car") && ((locked cursorObject) isEqualTo 0))']);
		life_actions pushBack (player addAction["<t color='#266ff7'>Descendre</t>",{player action ["Eject",(vehicle player)];},"",0,false,false,"",'(((vehicle player) isKindOf "Car") && ((locked (vehicle player)) isEqualTo 2))']);
        life_actions pushBack (player addAction["<t color='#FF0000'>Prendre l'objet</t>",{deleteVehicle cursorObject},"",0,false,false,"","cursorObject getVariable ['poserbyplayer',false]"]);
        life_actions pushBack (player addAction["<t color='#0099FF'>S'asseoir</t>",{[cursorObject,player] execVM "script\Sitting\sitdown.sqf"},true,1,true,true,"",' player distance cursorObject < 3 && {(("bench" in (str cursorObject)) || ("chair" in (str cursorObject)))} ']);
    };

    case independent: { 
        life_actions pushBack (player addAction["<t color='#FF0000'>Saisir l'objet</t>",{deleteVehicle cursorObject},"",0,false,false,"","cursorObject getVariable ['poserbyplayer',false]"]);
        life_actions pushBack (player addAction["<t color='#266ff7'>Déverouiller le véhicule</t>",{cursorObject lock false;},"",0,false,false,"",'((cursorObject isKindOf "Car") && ((locked cursorObject) isEqualTo 2))']);
		life_actions pushBack (player addAction["<t color='#266ff7'>Vérouiller le véhicule</t>",{cursorObject lock true;},"",0,false,false,"",'((cursorObject isKindOf "Car") && ((locked cursorObject) isEqualTo 0))']);
		life_actions pushBack (player addAction["<t color='#266ff7'>Descendre</t>",{player action ["Eject",(vehicle player)];},"",0,false,false,"",'(((vehicle player) isKindOf "Car") && ((locked (vehicle player)) isEqualTo 2))']);
        life_actions pushBack (player addAction["<t color='#FF0000'>Prendre l'objet</t>",{deleteVehicle cursorObject},"",0,false,false,"","cursorObject getVariable ['poserbyplayer',false]"]);
        life_actions pushBack (player addAction["<t color='#0099FF'>S'asseoir</t>",{[cursorObject,player] execVM "script\Sitting\sitdown.sqf"},true,1,true,true,"",' player distance cursorObject < 3 && {(("bench" in (str cursorObject)) || ("chair" in (str cursorObject)))} ']);
	};
};